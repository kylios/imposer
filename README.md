Imposer
=====

Imposing is the process of laying out the pages of a book to be printed. Often, books are printed and bound in _signatures_, or groups of _folios_ (folded sheets of paper). A signature may consist of 4, 5, 6, 8, or 12 folios - it completely depends on the project and the binder. If you wish to print and bind a book by hand, you will need to decide how many folios you want in each signature, and make sure your pages come out of the printer the right way.

Programs like _Microsoft Word_ have an option to print your document like a book. It will reconfigure each page into signatures, and print four pages to each sheet - two on the front and two on the back. This is how you want your document to look. If your signature has four folios, then you would take those first four sheets of paper, fold them each in half, stack them in the correct order, and there you would have the first sixteen pages of your book. Repeat this for all the rest of the pages, then continue binding.

Unfortunately, this feature isn't as common among other word processor programs, hence why I wrote this python library. Given a PDF document, _imposer_ will rearrange the pages depending on the desired number of folios per signature. You can then print your document, front and back, two pages per side.

# Usage

_Imposer_ is relatively easy to use. First, install it using `pip`:

```
pip install /path/to/imposer
```

Or just navigate into the `imposer` directory that was created when you checked it out from git.

Running _imposer_ is straightforward:

```
python -m imposer path/to/document.pdf -o path/to/output/document.pdf -s <folios_per_signature>
```

Just substitute `folios_per_signature` with the number of folios you'd like in each of your signatures and adjust the paths accordingly (careful, the output document **will** be overwritten without warning).

# Developing

Hopefully it's not hard to figure out how _imposer_ works. If you'd like to make changes, it's pretty easy. Here's what I do:

Create a virtualenv:

```
virtualenv _venv
source ./_venv/bin/activate
```

Now you can install _imposer_ into that virtualenv in "editable" mode:

```
pip install -r requirements.txt
```

There are unit tests as well. Running them is easy:

```
python -m unittest discover
```

# Contributing

Feel free! I welcome merge requests, or if you come across a bug or want a new feature, feel free to create an issue.

# The Future

There are some things I might consider adding:

- The ability to merge multiple PDFs into one
- If merging PDFs, allow changing their orientation, for double books (the kind where you flip it over and it's a different book)
- A friendly user interface
- Actually place two pages on one page in the correct place, so you don't have to depend on your printer to do this the right way.
