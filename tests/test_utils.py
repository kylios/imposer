#!/usr/bin/env python3

import unittest
from unittest import mock
from dataclasses import dataclass, field

from imposer import utils


class TestGenerateSignatures(unittest.TestCase):

	def test_generate_signatures(self):

		signature_size = 4
		pages = range(0, signature_size * 3)

		signatures = utils.generate_signatures(pages, signature_size)
		self.assertEqual(
			list(signatures),
			[
				[0, 1, 2, 3],
				[4, 5, 6, 7],
				[8, 9, 10, 11]
			]
		)

	def test_generate_uneven_signatures(self):

		signature_size = 4
		pages = range(0, signature_size * 3 - 2)

		signatures = utils.generate_signatures(pages, signature_size)
		self.assertEqual(
			list(signatures),
			[
				[0, 1, 2, 3],
				[4, 5, 6, 7],
				[8, 9]
			]
		)


class TestSplitPages(unittest.TestCase):

	def test_split_pages_even(self):
		# Create a list from 0-10
		pages = range(0, 10)

		# We should get back 2 generators:
		# front: should have the odd-numbered pages
		# back: should have the even-numbered pages
		front, back = utils.split_pages(pages)

		self.assertEqual(list(front), [0, 2, 4, 6, 8])
		self.assertEqual(list(back), [1, 3, 5, 7, 9])

	def test_split_pages_odd(self):
		# Create a list from 0-11
		pages = range(0, 11)

		# We should get back 2 generators:
		# front: should have the odd-numbered pages
		# back: should have the even-numbered pages
		front, back = utils.split_pages(pages)

		self.assertEqual(list(front), [0, 2, 4, 6, 8, 10])
		self.assertEqual(list(back), [1, 3, 5, 7, 9])



class TestOriginalPage(unittest.TestCase):

	def _assert_original_page(self, assertions):
		for assertion in assertions:
			self.assertEqual(
				utils.original_page(*assertion[0]),
				assertion[1],
				"utils.original_page({},{}) != {}".format(
					assertion[0][0],
					assertion[0][1],
					assertion[1]
				)
			)

	def test_original_page_first_signature(self):
		assertions = (
			# signature_size, page_num, original_page_num
			((4, 0), 15),
			((4, 1), 0),
			((4, 2), 1),
			((4, 3), 14),
			((4, 4), 13),
			((4, 5), 2),
			((4, 6), 3),
			((4, 7), 12),
			((4, 8), 11),
			((4, 9), 4),
			((4, 10), 5),
			((4, 11), 10),
			((4, 12), 9),
			((4, 13), 6),
			((4, 14), 7),
			((4, 15), 8),
		)

		self._assert_original_page(assertions)

	def test_original_page_front_sides(self):
		assertions = (
			# signature_size, page_num, original_page_num
			((4, 0), 15),
			((4, 1), 0),
			((4, 4), 13),
			((4, 5), 2),
			((4, 16), 31),
			((4, 17), 16),
			((4, 20), 29),
			((4, 21), 18),
			((4, 24), 27),
			((4, 25), 20),
			((4, 32), 47),
			((4, 33), 32),
			((4, 36), 45),
			((4, 37), 34),
		)
		self._assert_original_page(assertions)

	def test_original_page_back_sides(self):
		assertions = (
			((4, 2), 1),
			((4, 3), 14),
			((4, 6), 3),
			((4, 7), 12),
			((4, 18), 17),
			((4, 19), 30),
			((4, 22), 19),
			((4, 23), 28),
			((4, 34), 33),
			((4, 35), 46),
		)
		self._assert_original_page(assertions)

	def test_original_page_signature_sizes(self):
		assertions = (
			((6, 0), 23), ((6, 1), 0),
			((6, 2), 1), ((6, 3), 22),
			((6, 4), 21), ((6, 5), 2),
			((6, 6), 3), ((6, 7), 20),
			((6, 8), 19), ((6, 9), 4),
			((6, 10), 5), ((6, 11), 18),
			((6, 12), 17), ((6, 13), 6),
			((6, 14), 7), ((6, 15), 16),
			((6, 16), 15), ((6, 17), 8),
			((6, 18), 9), ((6, 19), 14),
			((6, 20), 13), ((6, 21), 10),
			((6, 22), 11), ((6, 23), 12),
			((6, 24), 47), ((6, 25), 24),
			((6, 26), 25), ((6, 27), 46),
			((5, 0), 19), ((5, 1), 0),
			((5, 2), 1), ((5, 3), 18),
			((5, 20), 39), ((5, 21), 20),
			((5, 22), 21), ((5, 23), 38),
		)
		self._assert_original_page(assertions)


class TestCombinePdfs(unittest.TestCase):

	def test_combine_one_start_0(self):
		mock_pdf1 = mock.Mock()
		mock_pdf1.start_page = 0
		mock_pdf1.fp.getNumPages.return_value = 10
		mock_pdf1.fp.getPage.side_effect = lambda page_num: 'Page{}'.format(page_num)

		result = utils.combine_pdfs([mock_pdf1])
		self.assertEqual(
			list(result),
			[
				'Page0',
				'Page1',
				'Page2',
				'Page3',
				'Page4',
				'Page5',
				'Page6',
				'Page7',
				'Page8',
				'Page9'
			]
		)

	def test_combine_one_start_offset(self):
		mock_pdf1 = mock.Mock()
		mock_pdf1.start_page = 2
		mock_pdf1.fp.getNumPages.return_value = 10
		mock_pdf1.fp.getPage.side_effect = lambda page_num: 'Page{}'.format(page_num)

		result = utils.combine_pdfs([mock_pdf1])
		self.assertEqual(
			list(result),
			[
				'Page2',
				'Page3',
				'Page4',
				'Page5',
				'Page6',
				'Page7',
				'Page8',
				'Page9'
			]
		)


	def test_combine_multiple(self):
		mock_pdf1 = mock.Mock()
		mock_pdf1.start_page = 2
		mock_pdf1.fp.getNumPages.return_value = 10
		mock_pdf1.fp.getPage.side_effect = lambda page_num: '1Page{}'.format(page_num)

		mock_pdf2 = mock.Mock()
		mock_pdf2.start_page = 0
		mock_pdf2.fp.getNumPages.return_value = 8
		mock_pdf2.fp.getPage.side_effect = lambda page_num: '2Page{}'.format(page_num)

		result = utils.combine_pdfs([mock_pdf1, mock_pdf2])

		self.assertEqual(
			list(result),
			[
				'1Page2',
				'1Page3',
				'1Page4',
				'1Page5',
				'1Page6',
				'1Page7',
				'1Page8',
				'1Page9',
				'2Page0',
				'2Page1',
				'2Page2',
				'2Page3',
				'2Page4',
				'2Page5',
				'2Page6',
				'2Page7',
			]
		)





if __name__ == '__main__':
	unittest.main()
