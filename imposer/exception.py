class ImposerException(Exception):
    MESSAGE = 'DO NOT INSTANTIATE `ImposerException`'

    def __init__(self, **kwargs):
        self.args = kwargs

        message = self.MESSAGE
        if len(kwargs):
            message += ': ' + ', '.join(map(
                lambda arg: '{}={}'.format(arg[0], arg[1]),
                kwargs.items()
            ))

        super(ImposerException, self).__init__(message)
