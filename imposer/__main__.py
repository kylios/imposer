import argparse
import logging
import textwrap
from dataclasses import dataclass

from .project import Project, Job, PAGE_SIZES

logging.basicConfig(level=logging.DEBUG)


LOGGER = logging.getLogger(__name__)


def parse_args():
    parser = argparse.ArgumentParser(
        description=textwrap.dedent('''
        Imposes a PDF document so that it can be bound into a book.
        '''))

    parser.add_argument(
        'path',
        type=str, nargs='+',
        help=('The path to a PDF file. Arguments may include the '
              'page number to start with (0-indexed), like so: `path:page_num`')
    )

    parser.add_argument(
        '--signature_size', '-s',
        type=int, required=False, default=0,
        help='The number of folios in a signature'
    )

    parser.add_argument(
        '--project_name', '-o',
        type=str, required=True,
        help='The file to write to')

    parser.add_argument(
        '--two_sides', '-t',
        required=False, default=False, action='store_true',
        help='Generate one document per side (front and back)')

    parser.add_argument(
        '--pages_per_sheet', '-p',
        type=int, required=False, default=1,
        help='The number of pages per sheet')

    parser.add_argument(
        '--output_page_size', '-d',
        type=str, required=False, default='Letter',
        choices=PAGE_SIZES.keys(),
        help='The size of the output pages'
    )

    parser.add_argument(
        '--signature_mark', '-m',
        type=int, required=False, default=0,
        help='The width in pixels for the signature mark'
    )

    parser.add_argument(
        '--limit_signatures', '-l',
        nargs='+', required=False, default=None,
        help='Only generate the given signatures. Signatures are 0-indexed.'
    )

    return parser.parse_args()


def main():
    args = parse_args()

    LOGGER.info('Opening project %s', args.project_name)
    project = Project(args.project_name)
    project.open()

    if args.limit_signatures:
        args.limit_signatures = [int(sig_num) for sig_num in args.limit_signatures]

    job = Job(
        paths=args.path,
        signature_size=args.signature_size,
        pages_per_sheet=args.pages_per_sheet,
        two_sides=args.two_sides,
        output_page_size=PAGE_SIZES[args.output_page_size],
        signature_mark=args.signature_mark,
        limit_signatures=args.limit_signatures
    )

    LOGGER.info('Beginning job...')
    project.run(job)


if __name__ == '__main__':
    main()
