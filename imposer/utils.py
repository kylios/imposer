import io
import logging
import math

import PyPDF2
from reportlab.lib import colors
from reportlab.pdfgen import canvas

LOGGER = logging.getLogger(__name__)


def open_file(path):
    LOGGER.info('Opening {}'.format(path))
    f = open(path, 'rb')
    reader = PyPDF2.PdfFileReader(f)
    return reader


def page_size(page):
    return (page.mediaBox.getWidth(), page.mediaBox.getHeight())


def user_space_to_mm(value):
    # PDFs represent page size in 'user space units,' which
    # is 1/72 of an inch.
    # Get the size and convert to inches.
    inches = value / 72.0

    # Convert to mm
    return inches * 25.4


def mm_to_user_space(value):
    inches = value / 25.4
    return inches * 72


def scaled_page(page, width, height):
    '''
    Returns a new page that is `page` scaled to `width` and `height`.
    `width` and `height` are in user space units.
    '''
    new_page = PyPDF2.pdf.PageObject.createBlankPage(width=width, height=height)

    page_width, page_height = page_size(page)

    # Get the scaling factor. We want to preserve the page's aspect ratio, so
    # the factor will be determined by comparing the page's width and height
    # to the desired width and height, and choosing the smallest ratio.
    factor = min(float(width) / float(page_width), float(height) / float(page_height))

    new_page.mergeScaledPage(page, factor)
    return new_page


def combine_pdfs(pdfs):
    for pdf in pdfs:
        stop_page = pdf.stop_page if pdf.stop_page is not None else pdf.fp.getNumPages()
        for page_num in range(pdf.start_page, stop_page):
            yield pdf.fp.getPage(page_num)


def generate_signature_pages(combined_pdf, signature_size):
    '''
    Generates `Page` objects in the order they must be
    printed according to `signature_size`.
    '''
    pages_per_signature = signature_size * 4
    num_pages = len(combined_pdf)
    LOGGER.debug('There are %d total pages in the PDFs', num_pages)

    num_signatures = int(math.ceil(num_pages / 4 / signature_size))
    total_num_pages = num_signatures * signature_size * 4

    LOGGER.debug('There will be %d total pages in the final document', total_num_pages)

    for i in range(total_num_pages):
        original_page_num = original_page(signature_size, i)
        if original_page_num >= num_pages:
            LOGGER.debug(
                'Generating a blank page for %d. Original page was %d',
                i,
                original_page_num,
            )
            page = PyPDF2.pdf.PageObject.createBlankPage(
                width=combined_pdf[0].mediaBox.getWidth(),
                height=combined_pdf[0].mediaBox.getHeight(),
            )
            yield page
        else:
            yield combined_pdf[original_page_num]

    while num_pages < num_pages:
        LOGGER.debug(
            'Generating a blank page for %d, out of %d', num_pages, total_num_pages
        )
        page = PyPDF2.pdf.PageObject.createBlankPage(
            width=combined_pdf[0].mediaBox.getWidth(),
            height=combined_pdf[0].mediaBox.getHeight(),
        )
        yield page

        num_pages += 1


def limit_pages(pages, stop_page):
    '''
    Yields `pages` up to `stop_page`.
    '''
    for i, page in enumerate(pages):
        if i < stop_page:
            yield page
        else:
            break


def combine_pages(pages, pages_per_sheet, output_page_size):
    '''
    Combines the given pages into new pages, depending
    on `pages_per_sheet` (currently only `1` or `2`
    supported). Returns a generator generating `Page`
    objects.
    '''
    if pages_per_sheet == 1:
        for page in pages:
            yield page

    elif pages_per_sheet == 2:
        try:
            while True:
                # Reset these now so that one or both of
                # them are set and `None` when `StopIteration`
                # is raised.
                page1 = None
                page2 = None

                page1 = scaled_page(
                    next(pages),
                    mm_to_user_space(output_page_size.height / 2.0),
                    mm_to_user_space(output_page_size.width)
                )
                page2 = scaled_page(
                    next(pages),
                    mm_to_user_space(output_page_size.height / 2.0),
                    mm_to_user_space(output_page_size.width)
                )

                page1_width, page1_height = page_size(page1)
                page2_width, page2_height = page_size(page2)

                page1_translation_x = page1_width
                page1_translation_y = page1_width

                page2_translation_x = page2_width / 2
                page2_translation_y = page2_width / 2

                # new_page = PyPDF2.pdf.PageObject.createBlankPage(width=page1_height, height=page1_width * 2)
                new_page = PyPDF2.pdf.PageObject.createBlankPage(
                    width=mm_to_user_space(output_page_size.width),
                    height=mm_to_user_space(output_page_size.height)
                )

                LOGGER.debug('Merging the left page')
                LOGGER.debug('  width: {}'.format(page1_width))
                LOGGER.debug('  height: {}'.format(page1_height))
                new_page.mergeRotatedTranslatedPage(
                    page1, 270, page1_translation_x, page1_translation_y, False
                )
                LOGGER.debug('Merging the right page')
                LOGGER.debug('  width: {}'.format(page2_width))
                LOGGER.debug('  height: {}'.format(page2_height))
                new_page.mergeRotatedTranslatedPage(
                    page2, 270, page2_translation_x, page2_translation_y, False
                )

                LOGGER.debug('New page: {}'.format(new_page))
                yield new_page

        except StopIteration:
            # If there's an odd number of pages, just merge the
            # last page.
            if page1 is not None:
                new_page = PyPDF2.pdf.PageObject.createBlankPage(
                    width=mm_to_user_space(output_page_size.width),
                    height=mm_to_user_space(output_page_size.height),
                )
                rotation = 270
                rx = page1.mediaBox.getWidth()
                ry = (
                    float(page1.mediaBox.getHeight())
                    - float(page1.mediaBox.getWidth()) / 2.0
                )
                new_page.mergeRotatedTranslatedPage(page1, rotation, rx, ry, False)
                yield new_page

            assert page2 is None

    else:
        raise Exception('Only 1 or 2 pages per sheet are currently supported.')


def generate_signatures(pages, signature_size, limit_signatures=None):
    '''
    Generate a series of lists of pages. Each list of pages is a single signature.
    They are generated in the order they should occur in the final book.

    Arguments:
        pages (iterable) - An iterable of `Page` objects
        signature_size (int) - The number of folios per signature
        limit_signatures (list[int]) - Optional. A list of signatures to generate.

    Generates:
        Each signature as a list of `Page` objects.
    '''
    if limit_signatures:
        LOGGER.debug('Limiting output to signatures: %s', str(limit_signatures))

    def _should_output_signature(sig_num):
        if not limit_signatures:
            return True
        if sig_num in limit_signatures:
            return True
        return False

    signature_num = 0
    signature = []
    for i, page in enumerate(pages):
        signature.append(page)

        if (i + 1) % signature_size == 0:
            if _should_output_signature(signature_num):
                LOGGER.debug('Output signature %d', signature_num)
                yield signature
            signature = []
            signature_num += 1

    # It's possible that the number of pages does not divide
    # evenly into `signature_size`. If there are any remaining
    # pages, then return them.
    if len(signature) and _should_output_signature(signature_num):
        LOGGER.debug('Output signature %d', signature_num)
        yield signature


def split_pages(pages):
    '''
    Return two generators from this single generator:
    one for the even pages, and one for the odd pages.
    '''
    even = list()
    odd = list()

    for i, page in enumerate(pages):
        if i % 2 == 0:
            even.append(page)
        else:
            odd.append(page)

    return even, odd


def write_pages(pages, filename):
    writer = PyPDF2.PdfFileWriter()

    for page in pages:
        writer.addPage(page)

    with open(filename, 'wb') as f:
        writer.write(f)


def original_page(signature_size, page_num):
    '''
    Return the page number in the original document of the page at `page_num`.
    `page_num` is the page in the new document, to which the page in the original document
    should be mapped.

    Args
        signature_size - The number of folios in each signature.
        page_num - The page number of the new document

    Returns
        The page number of the original document which should be mapped to the given page
        of the new document.
    '''
    pages_per_signature = 4 * signature_size

    folio = int(math.floor(page_num / 4)) % signature_size
    signature = int(math.floor(page_num / 4 / signature_size))

    signature_page_start = pages_per_signature * signature
    signature_page_end = pages_per_signature * (signature + 1) - 1

    LOGGER.debug(
        'signature_size=%d, page_num=%d, signature=%d, pages_per_signature=%d, folio=%d, signature_page_start=%d, signature_page_end=%d',
        signature_size,
        page_num,
        signature,
        pages_per_signature,
        folio,
        signature_page_start,
        signature_page_end,
    )

    if page_num % 4 == 0:  # front1
        original_page = signature_page_end - 2 * folio
    elif page_num % 4 == 1:  # front2
        original_page = signature_page_start + 2 * folio
    elif page_num % 4 == 2:  # back1
        original_page = signature_page_start + 2 * folio + 1
    else:  # back2
        original_page = signature_page_end - 2 * folio - 1

    LOGGER.debug(
        'Original page for %d with signature size %d: %d',
        page_num,
        signature_size,
        original_page,
    )
    return original_page


def draw_signature_marks(mark_width, signatures, num_signatures):
    for i, sig in enumerate(signatures):
        if mark_width > 0:
            # Draw a mark on the odd of the outermost folio in the signature.
            # This helps ensure that the signatures are in the right order.
            # Should be the last folio in the signature now that it's been
            # reversed.
            folio = sig[0]
            mark_page = get_mark_page(
                folio.mediaBox.getWidth(),
                folio.mediaBox.getHeight(),
                mark_width,
                i,
                num_signatures,
            )
            folio.mergePage(mark_page)

        yield sig


def get_mark_page(width, height, mark_width, sig_num, num_sigs):
    '''
    Returns a page that contains a signature mark. This page can
    be merged with another PDF page.

    The mark's length is calculated based on which signature this is
    in the stack and how many signatures there are in total. The mark's
    length is determined so that the marks will span the entire length
    of the spine once the signatures are combined.

    Arguments:
        width - The width of the page
        height - The height of the page
        mark_width - The width of the mark, in user units
        sig_num - The number of the signature, respective to the ordering
            of signatures in the text block.
        num_sigs - The total number of signatures in the text block
    '''
    # ReportLab docs
    # https://www.reportlab.com/docs/reportlab-userguide.pdf

    # This is confusing. The caller will think of this as the 'width'
    # because it is how wide the mark is. However, since the mark is
    # being drawn parallel to the horizon, we'll convert the 'width'
    # into height, respective to how it will be drawn on the document.
    mark_height = mark_width
    mark_y = float(height - mark_height) / 2.0

    # Figure out the size the mark needs to be
    mark_width = float(width) / num_sigs
    mark_x = sig_num * mark_width

    packet = io.BytesIO()
    can = canvas.Canvas(packet, pagesize=(width, height))

    can.setStrokeColor(colors.black)
    can.setFillColor(colors.black)
    can.rect(mark_x, mark_y, mark_width, mark_height, stroke=True, fill=True)

    can.save()
    packet.seek(0)

    # Convert it to a pypdf object
    pdf = PyPDF2.PdfFileReader(packet)
    return pdf.getPage(0)
