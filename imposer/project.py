import math
import os.path
import shutil
from collections import namedtuple
from dataclasses import dataclass, field
from io import IOBase
from typing import List

from . import utils
from .exception import ImposerException


@dataclass
class PageSize:
    '''
    width and height are expressed in mm.
    '''

    name: str
    width: float
    height: float


# TODO: complete the list:
# https://www.prepressure.com/library/paper-size
PAGE_SIZES = {
    'A0': PageSize(name='A0', width=841, height=1189),
    'A1': PageSize(name='A1', width=594, height=841),
    'A2': PageSize(name='A2', width=420, height=594),
    'A3': PageSize(name='A3', width=297, height=420),
    'A4': PageSize(name='A4', width=210, height=297),
    'A5': PageSize(name='A5', width=148, height=210),
    'A6': PageSize(name='A6', width=105, height=148),
    'A7': PageSize(name='A7', width=74, height=105),
    'A8': PageSize(name='A8', width=52, height=74),
    'Letter': PageSize(name='Letter', width=215.9, height=279.4)
}


@dataclass
class Job:
    paths: List[str] = field(default_factory=list)
    signature_size: int = 0
    pages_per_sheet: int = 1
    two_sides: bool = False
    output_page_size: PageSize = field(default_factory=lambda: PAGE_SIZES['Letter'])
    signature_mark: int = 0
    limit_signatures: List[int] = None


@dataclass
class PdfIn:
    fp: IOBase = None
    start_page: int = 0
    stop_page: int = None


class JobPathExists(ImposerException):
    MESSAGE = 'This job already exists'


class Project(object):
    def __init__(self, name):
        self.name = name

        self.BASE_DIR = os.path.join('./', name)

    def open(self):
        if not os.path.exists(self.BASE_DIR):
            os.mkdir(self.BASE_DIR)

    def _get_job_directory(self, job):
        return self.BASE_DIR

    def _setup_job_directory(self, dir):
        if os.path.exists(dir):
            shutil.rmtree(dir)

        os.mkdir(dir)

    def _run_job(self, job, job_dir):
        pdfs = []
        for path in job.paths:
            start_page = 0
            stop_page = None
            if len(path.split(':')) > 2:
                stop_page = int(path.split(':')[2])
            if len(path.split(':')) > 1:
                start_page = int(path.split(':')[1])

            pdfs.append(
                PdfIn(
                    fp=utils.open_file(path.split(':')[0]),
                    start_page=start_page,
                    stop_page=stop_page,
                )
            )

        pages = list(utils.combine_pdfs(pdfs))
        if job.signature_size > 0:
            pages = utils.generate_signature_pages(pages, job.signature_size)
        pages = utils.combine_pages(pages, job.pages_per_sheet, job.output_page_size)

        if job.two_sides:
            even, odd = utils.split_pages(pages)
            num_signatures = math.ceil(len(even) / float(job.signature_size))

            odd_signatures = utils.generate_signatures(
                odd, job.signature_size, job.limit_signatures
            )
            even_signatures = utils.draw_signature_marks(
                job.signature_mark,
                utils.generate_signatures(
                    even, job.signature_size, job.limit_signatures
                ),
                num_signatures,
            )

            padding = int(math.log10(len(even) / job.signature_size)) + 1
            for i, signature in enumerate(even_signatures):
                utils.write_pages(
                    signature,
                    os.path.join(
                        job_dir, ('even-sig{:0' + str(padding) + 'd}.pdf').format(i)
                    ),
                )

            padding = int(math.log10(len(odd) / job.signature_size)) + 1
            for i, signature in enumerate(odd_signatures):
                # Reverse the odd pages on the signature
                signature.reverse()

                utils.write_pages(
                    signature,
                    os.path.join(
                        job_dir, ('odd-sig{:0' + str(padding) + 'd}.pdf').format(i)
                    ),
                )

            utils.write_pages(even, os.path.join(job_dir, 'even-combined.pdf'))

            # Reverse all odd pages
            odd.reverse()
            utils.write_pages(odd, os.path.join(job_dir, 'odd-combined.pdf'))

        else:
            # This needs to become a list so we can get the length
            pages = list(pages)
            # Multiply the signature size by two because it is dealing with
            # even and odd pages.
            signature_size = 2 * job.signature_size

            num_signatures = math.ceil(len(pages) / float(signature_size))

            padding = int(math.log10(len(pages) / signature_size)) + 1
            signatures = utils.draw_signature_marks(
                job.signature_mark,
                utils.generate_signatures(pages, signature_size),
                num_signatures,
            )

            for i, signature in enumerate(signatures):
                utils.write_pages(
                    signature,
                    os.path.join(
                        job_dir, ('sig{:0' + str(padding) + 'd}.pdf').format(i)
                    ),
                )

            utils.write_pages(pages, os.path.join(job_dir, 'single-combined.pdf'))

    def run(self, job):
        job_dir = self._get_job_directory(job)
        self._setup_job_directory(job_dir)

        self._run_job(job, job_dir)
