from setuptools import setup

setup(
	name='imposer',
    version='0.1.0',
    packages=[
	    'imposer'
    ],
    entry_points={
        'console_scripts': [
            'imposer = imposer.__main__:main'
        ]
    },
    install_requires=[
        'PyPDF2',
        'reportlab'
    ]
)
